import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;

public class GETMyPostsTest {
    private final String token = "6723e3e94c3b69186548d4722c83cca9";
    private final String postsURL = "https://test-stand.gb.ru/api/posts";

    @Test
    void getASCTest() {
        String c;
        final String responseOne = given()
                .header("X-Auth-Token", token)
                //.queryParam("owner", "n")
                .queryParam("order", "ASC")
                .queryParam("page", "1")
                .log()
                .all()
                .when()
                .get(postsURL)
                .prettyPeek()
                .then().
                assertThat()
                .statusCode(200)
                .extract()
                .response()
                .jsonPath()
                .getString("data.id");


        String[] idString = responseOne.split(", ");

        System.out.println(responseOne);

        int[] idInt = new int[idString.length];
        for (int i = 1; i < idString.length - 1; i++) {
            idInt[i] = Integer.parseInt(idString[i]);
        }

        int a = idInt[1];
        int b = idInt[2];
        if (a < b) {
            c = "true";
        } else {
            c = "false";
        }
        assertThat(c, equalTo("true"));
    }

    public GETMyPostsTest() {
        super();
    }

    @Test
    void getDESCTest() {
        String c;
        final String responseOne = given()
                .header("X-Auth-Token", token)
                //.queryParam("owner", "n")
                .queryParam("order", "DESC")
                .queryParam("page", "1")
                .log()
                .all()
                .when()
                .get(postsURL)
                .prettyPeek()
                .then().
                assertThat()
                .statusCode(200)
                .extract()
                .response()
                .jsonPath()
                .getString("data.id");

        String[] idString = responseOne.split(", ");

        System.out.println(responseOne);

        int[] idInt = new int[idString.length];
        for (int i = 1; i < idString.length - 1; i++) {
            idInt[i] = Integer.parseInt(idString[i]);
        }

        int a = idInt[1];
        int b = idInt[2];
        if (a > b) {
            c = "true";
        } else {
            c = "false";
        }
        assertThat(c, equalTo("true"));
    }

    @Test
    void getNotAuthTest() {
        given()
                .queryParam("order", "ASC")
                .queryParam("page", "2")
                .log()
                .all()
                .when()
                .get(postsURL)
                .prettyPeek()
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    void getNullPageTest() {
        given()
                .header("X-Auth-Token", token)
                .queryParam("order", "ASC")
                .queryParam("page", "10000")
                .log()
                .all()
                .when()
                .get(postsURL)
                .prettyPeek()
                .then()
                .assertThat()
                .statusCode(200)
                .body("data.id[0]", nullValue())
                .body("data.authorId[0]", nullValue());
    }

    @Test
    void getNullPageNullTest() {
        given()
                .header("X-Auth-Token", token)
                .queryParam("order", "ASC")
                .queryParam("page", "0")
                .log()
                .all()
                .when()
                .get(postsURL)
                .prettyPeek()
                .then()
                .assertThat()
                .statusCode(200)
                .body("data.id[0]", nullValue())
                .body("data.authorId[0]", nullValue());
    }
}
