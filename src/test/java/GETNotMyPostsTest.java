import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;

public class GETNotMyPostsTest {
    private final String token = "6723e3e94c3b69186548d4722c83cca9";
    private final String postsURL = "https://test-stand.gb.ru/api/posts";

    @Test
    void getASCNotMeTest() {
        String c;
        final String responseOne = given()
                .header("X-Auth-Token", token)
                .queryParam("owner", "notMe")
                .queryParam("order", "ASC")
                .queryParam("page", "3")
                .log()
                .all()
                .when()
                .get(postsURL)
                .prettyPeek()
                .then().
                assertThat()
                .statusCode(200)
                .extract()
                .response()
                .jsonPath()
                .getString("data.id");

        String[] idString = responseOne.split(", ");


        int[] idInt = new int[idString.length];
        for (int i = 1; i < idString.length - 1; i++) {
            idInt[i] = Integer.parseInt(idString[i]);
        }

        int a = idInt[1];
        int b = idInt[2];
        if (a < b) {
            c = "true";
        } else {
            c = "false";
        }
        assertThat(c, equalTo("true"));
    }

    @Test
    void getDescNotMeTest() {
        String c;
        final String responseOne = given()
                .header("X-Auth-Token", token)
                .queryParam("owner", "notMe")
                .queryParam("order", "DESC")
                .queryParam("page", "3")
                .log()
                .all()
                .when()
                .get(postsURL)
                .prettyPeek()
                .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .response()
                .jsonPath()
                .getString("data.id");

        String[] idString = responseOne.split(", ");

        System.out.println(responseOne);

        int[] idInt = new int[idString.length];
        for (int i = 1; i < idString.length - 1; i++) {
            idInt[i] = Integer.parseInt(idString[i]);
        }

        int a = idInt[1];
        int b = idInt[2];
        if (a > b) {
            c = "true";
        } else {
            c = "false";
        }
        assertThat(c, equalTo("true"));
    }

    @Test
    void getAllNotMeTest() {
        String c;
        given()
                .header("X-Auth-Token", token)
                .queryParam("owner", "notMe")
                .queryParam("order", "ALL")
                .queryParam("page", "2")
                .log()
                .all()
                .when()
                .get(postsURL)
                .prettyPeek()
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    void getNullPageNotMeTest() {
        given()
                .header("X-Auth-Token", token)
                .queryParam("owner", "notMe")
                .queryParam("order", "ASC")
                .queryParam("page", "10000")
                .log()
                .all()
                .when()
                .get(postsURL)
                .prettyPeek()
                .then()
                .assertThat()
                .statusCode(200)
                .body("data.id[0]", nullValue())
                .body("data.authorId[0]", nullValue());
    }

    @Test
    void getNotAuthNotMeTest() {
        given()
                .queryParam("owner", "notMe")
                .queryParam("order", "ASC")
                .queryParam("page", "2")
                .log()
                .all()
                .when()
                .get(postsURL)
                .prettyPeek()
                .then()
                .assertThat()
                .statusCode(200);
    }

    public GETNotMyPostsTest() {
        super();
    }
}
