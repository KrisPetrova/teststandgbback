import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class POSTLoginTest {

        private final String token = "6723e3e94c3b69186548d4722c83cca9";
        private final String username = "NininaNina";
        private final String password = "9a013a1f14";
        private final String loginURL = "https://test-stand.gb.ru/gateway/login";

        @Test
        void postLoginTest() {
            final String response = given()
                    .formParam("username", username)
                    .formParam("password", password)
                    .log()
                    .all()
                    .when()
                    .post(loginURL)
                    .prettyPeek()
                    .then()
                    .assertThat()
                    .statusCode(200)
                    .extract()
                    .asString();
            assertThat(response, containsString("token"));
        }

        @Test
        void negativePostLoginTest() {
            final String response =
                    given()
                            .formParam("username", "zk")
                            .formParam("password", "zk")
                            .log()
                            .all()
                            .when()
                            .post(loginURL)
                            .prettyPeek()
                            .then()
                            .assertThat()
                            .statusCode(200)
                            .extract()
                            .asString();
            assertThat(response, containsString("Неправильный логин. Может быть не менее 3 и не более 20 символов"));
        }

        @Test
        void negativePostLoginTest2() {
            final String response =
                    given()
                            .formParam("username", username)
                            .formParam("password", "")
                            .log()
                            .all()
                            .when()
                            .post(loginURL)
                            .prettyPeek()
                            .then()
                            .assertThat()
                            .statusCode(200)
                            .extract()
                            .asString();
            assertThat(response, containsString("Неправильный логин. Может быть не менее 3 и не более 20 символов"));
        }

    @Test
    void negativePostLoginTest3() {
        final String response =
                given()
                        .formParam("username", "")
                        .formParam("password", password)
                        .log()
                        .all()
                        .when()
                        .post(loginURL)
                        .prettyPeek()
                        .then()
                        .assertThat()
                        .statusCode(200)
                        .extract()
                        .asString();
        assertThat(response, containsString("Неправильный логин. Может быть не менее 3 и не более 20 символов"));
    }

        public POSTLoginTest() {
            super();
        }

}
